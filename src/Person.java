import serialize.Test;
import serialize.XmlAttribute;
import serialize.XmlObject;
import serialize.XmlTag;

//@XmlObject(name = "person")
public class Person {
    private int age;

    @XmlTag
    Test test = new Test();

    @XmlTag
    private String name;

    @XmlTag
    private Person child;

    @XmlAttribute
    private int getAge() {
        return age;
    };

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, Person child) {
        this.name = name;
        this.age = age;
        this.child = child;
    }
}
