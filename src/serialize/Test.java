package serialize;

public class Test {
    public String name = "test";

    @Override
    public String toString() {
        return "Test{" +
                "name='" + name + '\'' +
                '}';
    }
}
