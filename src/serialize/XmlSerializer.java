package serialize;

import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.dom4j.tree.DefaultElement;
import org.dom4j.tree.DefaultText;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public class XmlSerializer {
    public Document serialize(Object object) throws NotXmlObjectException, AlreadyExistsException, InvalidMethodSignatureException {
        Document document = DocumentHelper.createDocument();

        XmlObject xmlObject = object.getClass().getAnnotation(XmlObject.class);

        if (xmlObject == null)
            throw new NotXmlObjectException();

        document.add(serializeObject(object, object.getClass()));

        return document;
    }

    private static Node serializeVal(Object object) throws AlreadyExistsException, InvalidMethodSignatureException {
        if (object == null) {
            return new DefaultText("");
        }

        XmlObject xmlObject = object.getClass().getAnnotation(XmlObject.class);

        if (xmlObject != null) {
            return serializeObject(object, object.getClass());
        } else {
            return new DefaultText(object.toString());
        }
    }

    private static <T> Element serializeObject(Object object, Class<T> cls) throws AlreadyExistsException, InvalidMethodSignatureException {
        XmlObject xmlObject = cls.getAnnotation(XmlObject.class);

        String name = xmlObject.name().isEmpty()
                ? cls.getSimpleName()
                : xmlObject.name();

        Element root = new DefaultElement(name);

        Class<?> parent = cls.getSuperclass();

        XmlObject parentXmlObject = parent.getAnnotation(XmlObject.class);

        if (parentXmlObject != null) {
            Element p = serializeObject(object, parent);

            for (Element el : p.elements()) {
                root.add(el.detach());
            }

            for (Attribute attr : p.attributes()) {
                root.addAttribute(attr.getName(), attr.getValue());
            }
        }

        for (Field field : cls.getDeclaredFields()) {
            if (field.getAnnotation(XmlTag.class) != null) {
                Element el = serializeTag(field, object);

                if (root.element(el.getName()) != null) {
                    throw new AlreadyExistsException();
                }

                root.add(el);
            }
        }

        for (Method method : cls.getDeclaredMethods()) {
            if (method.getAnnotation(XmlTag.class) != null) {
                if (method.getReturnType() == void.class
                    || method.getParameterCount() != 0) {
                    throw new InvalidMethodSignatureException();
                }

                Element el = serializeTag(method, object);

                if (root.element(el.getName()) != null) {
                    throw new AlreadyExistsException();
                }

                root.add(el);
            }
        }

        for (Field field : cls.getDeclaredFields()) {
            XmlAttribute xmlAttr = field.getAnnotation(XmlAttribute.class);

            if (xmlAttr != null) {
                Element tag = root;

                if (!xmlAttr.tag().isEmpty()) {
                    tag = root.element(xmlAttr.tag());
                }

                serializeAttr(field, object, tag);
            }
        }

        for (Method method : cls.getDeclaredMethods()) {
            XmlAttribute xmlAttr = method.getAnnotation(XmlAttribute.class);

            if (xmlAttr != null) {
                if (method.getReturnType() == void.class
                        || method.getParameterCount() != 0) {
                    throw new InvalidMethodSignatureException();
                }

                Element tag = root;

                if (!xmlAttr.tag().isEmpty()) {
                    tag = root.element(xmlAttr.tag());
                }

                serializeAttr(method, object, tag);
            }
        }

        return root;
    }

    private static Element serializeTag(Member member, Object object) throws AlreadyExistsException, InvalidMethodSignatureException {
        XmlTag xmlTag = null;

        if (member instanceof Method) {
            xmlTag = ((Method) member).getAnnotation(XmlTag.class);
        } else if (member instanceof Field) {
            xmlTag = ((Field) member).getAnnotation(XmlTag.class);
        }

        if (xmlTag == null) {
            return null;
        }

        String tagName = xmlTag.name().isEmpty()
                ? member.getName()
                : xmlTag.name();

        if (member instanceof Method && tagName.startsWith("get")) {
            tagName = Character.toLowerCase(tagName.charAt(3))
                    + tagName.substring(4);
        }

        Object val = null;

        if (member instanceof Method) {
            Method method = (Method) member;
            try {
                method.setAccessible(true);
                val = method.invoke(object);
            } catch (InvocationTargetException e) {

            } catch (IllegalAccessException e) { }
        } else if (member instanceof Field) {
            Field field = (Field) member;
            try {
                field.setAccessible(true);
                val = field.get(object);
            } catch (IllegalAccessException e) {
            }
        }

        Element el = new DefaultElement(tagName);
        el.add(serializeVal(val));
        return el;
    }

    private static void serializeAttr(Member member, Object object, Element tag) throws AlreadyExistsException {
        if (tag == null) {
            return;
        }

        XmlAttribute xmlAttr = null;

        if (member instanceof Method) {
            xmlAttr = ((Method) member).getAnnotation(XmlAttribute.class);
        } else if (member instanceof Field) {
            xmlAttr = ((Field) member).getAnnotation(XmlAttribute.class);
        }

        if (xmlAttr == null) {
            return;
        }

        String attrName = xmlAttr.name().isEmpty()
                ? member.getName()
                : xmlAttr.name();

        Object val = null;

        if (member instanceof Method) {
            Method method = (Method) member;
            try {
                method.setAccessible(true);
                val = method.invoke(object);
            } catch (InvocationTargetException e) {

            } catch (IllegalAccessException e) { }
        } else if (member instanceof Field) {
            Field field = (Field) member;
            try {
                field.setAccessible(true);
                val = field.get(object);
            } catch (IllegalAccessException e) {
            }
        }

        if (member instanceof Method && attrName.startsWith("get")) {
            attrName = Character.toLowerCase(attrName.charAt(3))
                    + attrName.substring(4);
        }

        if (tag.attribute(attrName) != null) {
            throw new AlreadyExistsException();
        }

        tag.addAttribute(attrName, val.toString());
    }
}
