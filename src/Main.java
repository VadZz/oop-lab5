import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import serialize.AlreadyExistsException;
import serialize.InvalidMethodSignatureException;
import serialize.NotXmlObjectException;
import serialize.XmlSerializer;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws InvalidMethodSignatureException, AlreadyExistsException, NotXmlObjectException, IOException {
        Person ivan = new Student("Ivan", 14, "ITMO");
       // Person petr = new Person("Petr", 104, ivan);

        Document xml = new XmlSerializer().serialize(ivan);

        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer;
        writer = new XMLWriter(System.out, format);
        writer.write(xml);
    }
}
