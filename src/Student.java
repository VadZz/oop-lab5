import serialize.XmlAttribute;
import serialize.XmlObject;
import serialize.XmlTag;

@XmlObject(name = "student")
public class Student extends Person {
    @XmlTag
    private String uni;

    public Student(String name, int age, String uni) {
        super(name, age);
        this.uni = uni;
    }

    @XmlAttribute
    String getUni() {
        return uni;
    }


}
